import java.util.*;

/**
 * Neco jako prepreavka, akorat s vice atributy
 */
public class P2File {

    public String fileName;
    public int[] pixels;
    public int capacity;
    public int width;
    public int height;
    public String notes;


    public P2File(String fileName, int[] data, int capacity, int width, int height, String notes) {
        this.fileName = fileName;
        this.pixels = data;
        this.width = width;
        this.height = height;
        this.capacity = capacity;
        this.notes = notes;
        //System.out.println("Files capacity " + capacity);
    }

    public String getFileName() {
        return fileName;
    }

    public int[] getPixels() {
        return pixels;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
