import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.*;
import java.awt.image.BufferedImage;

/**
 * Drawing panel vykreslujici mapu Plzne zobrazujici nejvyssi a nejnizsi body a body nejvyssiho stoupani
 *
 */
public class DrawingPanel extends JPanel {

    P2File file;
    BufferedImage img;
    //BufferedImage secondImg;
    int[] originalPixels;
    int indexHighestPoint;
    int indexLowestPoint;
    int indexBiggestClimb;
    int startX;
    int startY;
    double scale;
    private int VELIKOST_SIPKY = 45;
    private final int MAX_COLOR_VALUE = 255;

    /**
     * Konstruktor, nastavi preferovanou velikost a obsluhu mysi
     */
    public DrawingPanel() {
        this.setPreferredSize(new Dimension(640, 480));

        this.requestFocus();
        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                /*int mX = e.getX();
                int mY = e.getY();
                int colour = this.getRGB(mX, mY) >> 8 & 0xFF;
                System.out.println("On x=" + mX + ", y=" + mY + " is colour " + colour);*/
                System.out.println("[" + e.getX() + "," + e.getY() + "]");
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

    }

    /**
     * Metoda volana z mainu nad instanci panelu pro nacteni mapy z P2File
     * ulozi si do atributu indexy dulezitych bodu - ty jsou pak v jinych metodach prevadeny na souradnice
     * @param file pgm soubor obsahujici vsechny informace, jez jsme byli schopni nacist
     */
    public void gatherData(P2File file) {
        this.file = file;
        int[] pixels = file.getPixels();
        int length = pixels.length;
        this.originalPixels = new int[length];
        int highestPoint = 0;
        int lowestPoint = file.capacity;
        int biggestClimb = 0;

        // projede cely seznam, ulozi si do atributu originalPixels puvodni hodnoty pixelu a prescaluje to na maximalni barevnou capacity 0-255
        for (int i = 0; i < length; i++) {
            int pixel = pixels[i];
            originalPixels[i] = pixel;

            if (pixel > highestPoint) {
                highestPoint = pixel;
                indexHighestPoint = i;
            }
            if (pixel < lowestPoint) {
                lowestPoint = pixel;
                indexLowestPoint = i;
            }
            if (i-1 > 0) {
                int climb = Math.abs(pixel - pixels[i-1]);
                if (climb > biggestClimb) {
                    biggestClimb = climb;
                    indexBiggestClimb = i;
                }
            }
            if (i+1 < length) {
                int climb = Math.abs(pixel - pixels[i+1]);
                if (climb > biggestClimb) {
                    biggestClimb = climb;
                    indexBiggestClimb = i;
                }
            }
            if (i >= file.getWidth()) {
                int climb = Math.abs(pixel - pixels[i - file.getWidth()]);
                if (climb > biggestClimb) {
                    biggestClimb = climb;
                    indexBiggestClimb = i;
                }
            }
            if (i < (length-file.getWidth())) {
                int climb = Math.abs(pixel - pixels[i+ file.getWidth()]);
                if (climb > biggestClimb) {
                    biggestClimb = climb;
                    indexBiggestClimb = i;
                }
            }
        }

        for (int i = 0; i < length; i++) {
            double pixel = pixels[i];
            //System.out.print("original je: " + pixel);
            pixel = pixel/(double)file.capacity*MAX_COLOR_VALUE;
            //System.out.println("po prevodu je: " + pixel);
            pixels[i] = (int)pixel;
            pixels[i] = pixels[i] << 16 | pixels[i] << 8 | pixels[i];
        }

        // vykreslovani je pres tri barevne slozky -> vykrelsovani pres stupne sedi mi hodnotu 255 vykreslilo jako 220

        this.img = new BufferedImage(file.getWidth(), file.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        this.img.setRGB(0,0,img.getWidth(), img.getHeight(), file.pixels, 0,img.getWidth());
    }

    /**
     * Zakladni metoda drawing panelu
     * @param g graficky kontext
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;


        drawImage(g2);
        //System.out.println("Sirka obrazku " + img.getWidth() + ", vyska " + img.getWidth());
        //System.out.println("Sirka obrazovky " + this.getWidth() + ", vyska " + this.getHeight());
        drawPoints(g2);
    }

    /**
     * zavola vykreslovani jednotlivych sipek, preda jim vse potrebne
     *
     * @param g2 graficky kontext
     */
    private void drawPoints(Graphics2D g2) {
        g2.setColor(Color.RED);

        //System.out.println("Highest point:");
        calculateCoordAndDraw(indexHighestPoint, g2,"Highest point");
        //System.out.println("Lowest point:");
        calculateCoordAndDraw(indexLowestPoint, g2, "Lowest point");
        //System.out.println("Biggest climb:");
        calculateCoordAndDraw(indexBiggestClimb, g2, "Biggest elevation");
    }

    /**
     * Metoda spocte souradnice X a Y bodu diky indexu v poli pixelu
     *
     * @param point index daneho bodu
     * @param g2 graficky kontext
     * @param text popisek daneho bodz
     */
    private void calculateCoordAndDraw(int point, Graphics2D g2, String text) {
        int coordX = point % img.getWidth();
        int coordY = point / img.getWidth();

        // souradnice X nej bodu na scene
        int x = (int) (coordX*this.scale) + this.startX;
        // souradnice Y nej bodu na scene
        int y = (int) (coordY*this.scale) + this.startY;

        drawPoint(g2, x, y, text);
    }

    /**
     * Sebere si vsechny potrebne informace pro vykresleni sipky, nasledne pak zavola metodu pro vykresleni
     * s potrebnymi parametry
     *
     * @param g2 graficky kontext
     * @param m souradnice X daneho bodu (napr. nejvyssi misto na mape)
     * @param n souradnice Y toho stejneho bodu
     * @param text popisek daneho bodu
     */
    private void drawPoint(Graphics2D g2, int m, int n, String text) {
        int[] corner = pickCorner(m, n);
        //System.out.println("Corner: " + corner[0] + ", " + corner[1]);

        int[] coords = findCoordsOfArrowAndText(corner, m, n);

        drawArrow(coords[0], coords[1], coords[2], coords[3], 20, g2);

        g2.setFont(new Font("Calibri", Font.PLAIN, 12));
        FontMetrics fm = g2.getFontMetrics();
        g2.drawString(text, coords[4] - fm.stringWidth(text)/2, coords[5]);
        //System.out.println("Souradnice S=[" + m + "," + n + "], T=[" + coords[2] + "," + coords[3] + "]");
    }


    /**
     * Pro bod na mape vybere nejvzdalenejsi roh -> to bude smernice nasledne vykreslene sipky
     *
     * Bod je tak reprezentovan polem int, prislo mi zbytecne kvuli dvoum pouzitim sestavovat novou tridu pro bod
     *
     * @param x souradnice X bodu
     * @param y souradnice Y bodu
     * @return pole int, na indexu 0 vrati hodnotu X, na indexu 1 vrati hodnotu Y
     */
    private int[] pickCorner(int x, int y) {
        int[] corner = new int[2];
        if (this.getWidth()/2 > x) {
            corner[0] = (int) (img.getWidth()*scale + this.startX);
            if (this.getHeight()/2 > y) {
                corner[1] = (int) (scale*img.getHeight() + this.startY);
            }
            else {
                corner[1] = startY;
            }
        }
        else {
            corner[0] = this.startX;
            if (this.getHeight()/2 > y) {
                corner[1] = (int) (scale*img.getHeight() + this.startY);
            }
            else {
                corner[1] = this.startY;
            }
        }
        return corner;
    }

    /**
     * Nalezne konec sipky dane velikosti na zaklade dvou bodu
     *
     * @param corner jeden z bodu, v nasem pripade je to nejvzdalenejsi roh
     * @param xStred souradnice X pocatku sipky
     * @param yStred souradnice Y pocatku sipky
     * @return vrati pole o velikosti 6 - obsahuje totiz 3 body,
     * 1) pocatek sipky, index 0,1
     * 2) konec sipky, index 2,3
     * 3) stred popisku sipky, index 4,5
     */
    private int[] findCoordsOfArrowAndText(int[] corner, int xStred, int yStred) {
        double vzdalX = xStred - corner[0];
        double vzdalY = yStred - corner[1];
        double c = Math.sqrt(vzdalX*vzdalX + vzdalY*vzdalY);
        double pomer = c/VELIKOST_SIPKY;

        double x = vzdalX/pomer;
        double y = vzdalY/pomer;
        int[] result = new int[6];
        xStred -= x/Math.abs(x)*7;
        yStred -= y/Math.abs(y)*4;
        result[0] = xStred;
        result[1] = yStred;
        result[2] = (int) (xStred - x);
        result[3] = (int) (yStred - y);
        result[4] = (int) (result[2] - x/1.5);
        result[5] = (int) (result[3] - y/1.5);
        return result;
    }


    /**
     * Funkcni metoda, ktera vykresli sipku na platno
     * @param x1 souradnice X pocatku sipky
     * @param y1 souradnice Y pocatku sipky
     * @param x2 souradnice X konce sipky
     * @param y2 souradnice Y konce sipky
     * @param tl velikost ukazatelu
     * @param g2 graficky kontext
     */
    public void drawArrow(double x1, double y1, double x2, double y2, double tl, Graphics2D g2) {
        double uX = x2 - x1;
        double uY = y2 - y1;
        double uLen1 = 1/Math.sqrt(uX*uX + uY*uY);
        uX *= uLen1;
        uY *= uLen1;
        g2.setStroke(new BasicStroke(8, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER));
        g2.draw(new Line2D.Double(x1, y1, x2-5, y2-5));

        Path2D hrot = new Path2D.Double();

        double vX = uY*tl*0.5;
        double vY = -uX*tl*0.5;

        double cX = x1 + uX*tl;
        double cY = y1 + uY*tl;
        hrot.moveTo(vX + cX,vY + cY);
        hrot.lineTo(x1, y1);
        hrot.lineTo(cX - vX, cY - vY);

        g2.draw(hrot);
    }

    /**
     * Zkusebni metoda pro nacteni obrazku jineho nez .pgm
     * @param img vlozeny obrazek
     */
    public void setImage(BufferedImage img) {
        this.img = img;
    }

    /**
     * Metoda vykreslujici obrazek na platno,
     * vyuziva bilinearni interpolaci
     *
     * @param g2 graficky kontext
     */
    private void drawImage(Graphics2D g2) {
        // nastaveni pozadi
        g2.setColor(Color.BLACK);
        g2.fillRect(0,0, this.getWidth(), this.getHeight());

        double scaleX = ((double) this.getWidth())/img.getWidth();
        double scaleY = ((double) this.getHeight())/ img.getHeight();
        this.scale = Math.min(scaleX, scaleY);

        int newWidth = (int)(scale* img.getWidth());
        int newHeight = (int)(scale* img.getHeight());

        startX = this.getWidth()/2 - (newWidth/2);
        startY = this.getHeight()/2 - (newHeight/2);

        g2.setRenderingHints(new RenderingHints(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR));
        g2.drawImage(img, startX,startY, newWidth, newHeight, null);
    }
}
