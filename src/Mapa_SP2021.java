import javax.swing.JFrame;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class Mapa_SP2021 extends JFrame {

	private static DrawingPanel panel;
	static BufferedImage img;

	public static void main(String[] args) {

//-------Klasika----------------------------------------------

	 	Mapa_SP2021 okno = new Mapa_SP2021();
	 	okno.setTitle("Jiri Winter - A20B0275P");
	 	okno.setSize(640,480);
		panel = new DrawingPanel();
		okno.add(panel);
		okno.pack();

//--------------------------------------------------------------

//---------- Funkcni cast ---------------------------------------

		try {
			//System.out.println(args[0]);
			P2File file = readFile(args[0]);
			panel.gatherData(file);
			//img = ImageIO.read(new File("bin/data/fotoTest.jpg"));
			//panel.setImage(img);

		} catch(IOException e) {
			System.out.println("Soubor nebyl nacten...");
			System.exit(0);
		}

		okno.setDefaultCloseOperation(EXIT_ON_CLOSE);
		okno.setLocationRelativeTo(null);
		okno.setVisible(true);
	}


	/**
	 * Nacitani pgm souboru je hotovo
	 * @param fileName jmeno souboru, ktery chci nacist
	 * @return instanci tridy P2File - coz je soubor obsahujici data nacteneho souboru
	 * @throws IOException exception
	 */
	public static P2File readFile(String fileName) throws IOException {
		Scanner sc = new Scanner(new File(fileName));
		int[] pixels;

		//Kontrola, ze je soubor oznacen pomoci P2
		if (!sc.nextLine().contains("P2")) {
			System.out.println("Soubor neni typu P2...");
			throw new IOException("Soubor neni typu P2...");
		}
		String poznamky = "";
		int widthRes = 0;
		int heightRes = 0;
		boolean notes = true;
		while (notes) {
			String neco = sc.nextLine();
			if (!neco.contains("#")) {
				String[] res = neco.split(" ");
				widthRes = Integer.parseInt(res[0]);
				heightRes = Integer.parseInt(res[1]);
				notes = false;
			} else {
				poznamky += neco;
			}
		}
		//Rozliseni obrazu

		int capacity = sc.nextInt();
		pixels = new int[widthRes*heightRes];
		//Nacte vsechny hodnoty na jednotlivych pixelech
		int i = 0;
		while (sc.hasNextInt()) {
			pixels[i++] = sc.nextInt();
		}
		//System.out.println(Arrays.toString(pixels));
		sc.close();
		return new P2File(fileName, pixels, capacity, widthRes, heightRes, poznamky);
	}
}
